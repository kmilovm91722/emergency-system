package com.telematics.emergenysystem.activities;

import com.telematics.emergenysystem.R;
import com.telematics.emergenysystem.reporter.Reporter;
import com.telematics.emergenysystem.utilities.DialogFactory;

import android.location.LocationManager;
import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.View;

public class ReportListActivity extends Activity {
	private Reporter reporter;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_report_list);
		reporter = (Reporter) getApplication();
		LocationManager service = (LocationManager) getSystemService(LOCATION_SERVICE);
		boolean enabled = service
				.isProviderEnabled(LocationManager.GPS_PROVIDER);

		// Check if enabled and if not send user to the GSP settings
		// Better solution would be to display a dialog and suggesting to
		// go to the settings
		if (!enabled) {
			DialogFactory.getGPSEnableDialog(this).show();
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.activity_report_list, menu);
		return true;
	}

	public void onRobberyButtonClick(View view) {
		reporter.report("Robbery");
		DialogFactory.getReportConfirmDialog(this).show();
	}

	public void onAccidentButtonClick(View view) {
		reporter.report("Accident");
		DialogFactory.getReportConfirmDialog(this).show();
	}

	public void onNaturalDisasterButtonClick(View view) {
		Intent intent = new Intent(this, NaturalDisasterActivity.class);
		startActivity(intent);
	}
}
