package com.telematics.emergenysystem.activities;

import com.telematics.emergenysystem.R;
import com.telematics.emergenysystem.reporter.Reporter;
import com.telematics.emergenysystem.utilities.DialogFactory;

import android.os.Bundle;
import android.app.Activity;
import android.view.Menu;
import android.view.View;

public class NaturalDisasterActivity extends Activity {

	Reporter reporter;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_natural_disaster);
		reporter = (Reporter) getApplication();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.activity_natural_disaster, menu);
		return true;
	}

	public void onEarthquakeButtonClick(View view) {
		reporter.report("Earthquake");
		DialogFactory.getReportConfirmDialog(this).show();
	}

	public void onCollapseButtonClick(View view) {
		reporter.report("Collapse");
		DialogFactory.getReportConfirmDialog(this).show();
	}

	public void onFireButtonClick(View view) {
		reporter.report("Fire");
		DialogFactory.getReportConfirmDialog(this).show();
	}
}
