package com.telematics.emergenysystem.reporter;

import java.util.Date;

import com.telematics.emergenysystem.R;
import com.telematics.emergenysystem.utilities.WSClient;
import android.app.Application;
import android.content.Context;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.widget.Toast;

public class Reporter extends Application implements LocationListener {

	private LocationManager locationManager;
	private String provider;
	private Location location;

	public Location getLocation() {
		return location;
	}

	@Override
	public void onCreate() {
		super.onCreate();

		// Get the location manager
		locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
		// Define the criteria how to select the locatioin provider -> use
		// default
		Criteria criteria = new Criteria();
		provider = locationManager.getBestProvider(criteria, false);
		Location location = locationManager.getLastKnownLocation(provider);
		// Initialize the location fields
		if (location != null) {
			System.out.println("Provider " + provider + " has been selected.");
			onLocationChanged(location);
		}
	}

	public void report(String type) {
		double lat = location.getLatitude();
		double lng = location.getLongitude();
		Date date = new Date();
		String data = "latitude=" + lat + "&longitude=" + lng + "&time="
				+ date.getTime() + "&type=" + type;
		WSClient.invokeWebService((getString(R.string.web_service_report)),
				data);
	}

	/* Request updates */

	public void updateLocation() {
		locationManager.requestLocationUpdates(provider, 400, 1, this);
	}

	/* Remove the locationlistener updates when Activity is paused */

	public void pause() {
		locationManager.removeUpdates(this);
	}

	public void onLocationChanged(Location location) {
		this.location = location;
	}

	public void onStatusChanged(String provider, int status, Bundle extras) {
	}

	public void onProviderEnabled(String provider) {
		Toast.makeText(this, "Enabled new provider " + provider,
				Toast.LENGTH_SHORT).show();

	}

	public void onProviderDisabled(String provider) {
		Toast.makeText(this, "Disabled provider " + provider,
				Toast.LENGTH_SHORT).show();
	}

}
